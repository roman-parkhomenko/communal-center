import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getStorage, ref, listAll, getDownloadURL } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyB55ilGKv9jHXBaEON_4ewKtPUwBPudlgE",
  authDomain: "jk-centr.firebaseapp.com",
  projectId: "jk-centr",
  storageBucket: "jk-centr.appspot.com",
  messagingSenderId: "246614335764",
  appId: "1:246614335764:web:c8f55174956f1cf11ee201",
  measurementId: "G-V6LX1R194Y",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
getAnalytics(app);
const storage = getStorage(app);
const storageRef = ref(storage);

export type FileObjectType = {
  name: string;
  url: string;
  id?: number;
};
export type FolderContentsObject = {
  [key: string]: FileObjectType;
};

export async function getFolderContentsObject(
  folderName: string
): Promise<FolderContentsObject> {
  const folderRef = ref(storageRef, folderName);

  const fileRefs = await listAll(folderRef);

  const folderObject = fileRefs.items.reduce(async (acc, fileRef, index) => {
    const url = await getDownloadURL(fileRef);
    return {
      ...(await acc),
      [fileRef.name]: { name: fileRef.name, url, id: index },
    };
  }, {});

  return folderObject;
}

export async function getFileObject(filePath: string): Promise<FileObjectType> {
  const fileRef = ref(storageRef, filePath);
  const url = await getDownloadURL(fileRef);
  return { name: fileRef.name, url };
}
