import styled from "styled-components";

export const Image = styled.img`
  display: block;
  max-width: 100%;
  margin-bottom: 2rem;

  &:not(:first-child) {
    margin-top: 2rem;
  }
`;
export const GalleryContainer = styled.div`
  columns: 1;
  column-gap: 2rem;
  margin-top: 2rem;

  @media (min-width: 768px) {
    columns: 2;
  }

  @media (min-width: 992px) {
    columns: 3;
  }

  @media (min-width: 1400px) {
    columns: 4;
  }
`;
export const GalleryWrapper = styled.section`
  margin: auto;
  max-width: 90%;
`;
