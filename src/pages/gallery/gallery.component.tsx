import { useEffect } from "react";

import { useAppDispatch, useAppSelector } from "../../store/hooks";
import {
  fetchGalleryObject,
  selectGallery,
} from "../../store/gallery/gallery-uncategirized.slice";

import { GalleryContainer, GalleryWrapper, Image } from "./gallery.styles";

const Gallery = () => {
  const dispatch = useAppDispatch();
  const imagesObject = useAppSelector(selectGallery);
  useEffect(() => {
    dispatch(fetchGalleryObject());
  }, [dispatch]);

  const imagesList = Object.values(imagesObject);
  return (
    <GalleryWrapper>
      <h1>Фото галерея підприємства</h1>
      <GalleryContainer>
        {imagesList.map(({ name, url, id }) => (
          <Image
            key={id}
            src={url}
            alt={name}
            title={name}
            data-aos="zoom-out-left"
            data-aos-delay={id ? id * 50 : 0}
          />
        ))}
      </GalleryContainer>
    </GalleryWrapper>
  );
};
export default Gallery;
