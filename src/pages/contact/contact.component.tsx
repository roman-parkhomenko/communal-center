import MapCard from "../../components/map-card/map-card.component";
import { ContactContainer } from "./contact.styles";

const Contact = () => {
  return (
    <ContactContainer>
      <h2 data-aos="fade-left" data-aos-delay="100">
        КОНТАКТИ:
      </h2>
      <h3 data-aos="fade-left" data-aos-delay="200">
        КП "Житловий комунальний центр" Глухівської міської ради
      </h3>
      <address
        style={{ fontSize: "1.2rem" }}
        data-aos="fade-left"
        data-aos-delay="300"
      >
        Адреса:{" "}
        <span>
          вул. Путивльська, 33, м.Глухів, Шсткинський р-н, Сумська обл., 41400
        </span>
        <br />
        Електронна пошта:{" "}
        <a href="mailto:gvjrep1@ukr.net">gvjrep1@ukr.net &#9993;</a>
        <hr />
        <b>
          Директор КП ЖКЦ
          <br />
          Павлик Владислав Анатолійович
        </b>
        ,<br />
        <a href="tel:+380664335912">066-433-5912 &#9990;</a>
        <br />
        <a href="mailto:hlukhiv_energy@ukr.net">
          hlukhiv_energy@ukr.net &#9993;
        </a>
        <hr />
        <b>
          Головний інженер
          <br />
          Гридякін Микола Яковлевич
        </b>
        <br />
        <a href="tel:+380668162930">066-816-2930 &#9990;</a>
        <hr />
        <b>
          Головний бухгалтер <br />
          ???
        </b>
        <br />
        <a href="tel:+38???">??? &#9990;</a>
        <br />
        <a href="mailto:???">??? &#9993;</a>
      </address>
      <hr />
      <h3>
        Розташування на інтерактивній карті{" "}
        <i style={{ fontSize: "1.5em" }}>&#8595;</i>
      </h3>
      <MapCard />
    </ContactContainer>
  );
};
export default Contact;
