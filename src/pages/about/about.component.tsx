import { useAppDispatch } from "../../store/hooks";
import { useEffect } from "react";
import { fetchAboutImagesObject } from "../../store/about-images/about-images.slice";

const About = () => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(fetchAboutImagesObject());
  }, [dispatch]);
  return (
    <section>
      <h1>About</h1>
    </section>
  );
};
export default About;
