import { useLoaderData } from "react-router-dom";
import { sleep } from "../../App";

import { useEffect } from "react";
// import HomeHeaderCard from "../../components/home-header-card/home-header-card.component";
import HomeHeaderCarousel from "../../components/home-header/home-header.component";
import { useAppDispatch } from "../../store/hooks";
import { fetchHomeImagesObject } from "../../store/home-images/home-images.slice";

interface HomeLoaderData {
  date: string;
}

export async function homeLoader(): Promise<HomeLoaderData> {
  await sleep();
  return {
    date: new Date().toISOString(),
  };
}
function Home() {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(fetchHomeImagesObject());
  }, [dispatch]);

  let data = useLoaderData() as HomeLoaderData;
  return (
    <>
      <HomeHeaderCarousel />
      {/*<HomeHeaderCard />*/}
      {/*<Section*/}
      {/*  imgUrl={noLogo}*/}
      {/*  bgImgSizeContain*/}
      {/*  data-aos="fade-left"*/}
      {/*  data-aos-delay="100"*/}
      {/*/>*/}
      {/*<Section imgUrl={logo} data-aos="fade-right" />*/}
      {/*<Section*/}
      {/*  imgUrl={badPhoto.url}*/}
      {/*  data-aos="fade-right"*/}
      {/*  title={badPhoto.name}*/}
      {/*/>*/}
      <p style={{ display: "none" }}>Date from loader: {data.date}</p>
    </>
  );
}
export default Home;
