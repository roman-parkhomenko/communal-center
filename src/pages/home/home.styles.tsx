import styled from "styled-components";

type SectionProps = {
  imgUrl: string;
  bgImgSizeContain?: boolean;
};
export const Section = styled.section<SectionProps>`
  text-align: center;
  background: center no-repeat url(${({ imgUrl }) => imgUrl});
  background-size: ${({ bgImgSizeContain }) =>
    bgImgSizeContain ? "contain" : "cover"};
  min-height: calc(50vh - 2.5rem);
  box-shadow: inset 50px 0px 201px 0px black;

  @media screen and (min-width: 768px) {
    min-height: calc(100vh - 5rem);
  }
`;
