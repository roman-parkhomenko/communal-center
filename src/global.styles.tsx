import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
  /* Set default font and box-sizing */
  *,
  *::before,
  *::after {
    box-sizing: border-box;
    font-family: 'Roboto', sans-serif;
  }

  *::after {
    // dont know why but it resolves gap at the bottom of the page
    line-height: 0;
  }

  /* Remove default margin and padding */
  html,
  body,
  #root {
    margin: 0;
    padding: 0;
  }

  /* Set background color */
  body {
    background-color: #f5f5f5;
    overflow-y: overlay;
    scrollbar-gutter: stable;

    &::-webkit-scrollbar {
      width: 8px;
    }

    &::-webkit-scrollbar-track {
      background-color: gray;
    }

    &::-webkit-scrollbar-thumb {
      background-color: darkgrey;
      border-radius: 5px;
      box-shadow: 0 -100vh 0 100vh rgb(6, 54, 54), 0 0 15px 5px black;
    }

  }

  #root{
    
  }
  /* Set default font size and line height */
  html {
    font-size: 16px;
    line-height: 1.5;
  }

  /* Set default link styles */
  a {
    color: inherit;
    text-decoration: none;
  }

  /* Set default button styles */
  button {
    cursor: pointer;
    border: none;
    background-color: transparent;
    font-family: 'Roboto', sans-serif;
  }

  /* Set default heading styles */
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    margin: 0;
    font-family: 'Yeseva One', cursive;

    font-weight: 700;
    line-height: 1.2;
  }

  h1 {
    font-size: 2rem;
  }

  h2 {
    font-size: 1.75rem;
  }

  h3 {
    font-size: 1.5rem;
  }

  h4 {
    font-size: 1.25rem;
  }

  h5 {
    font-size: 1.15rem;
  }

  h6 {
    font-size: 1rem;
  }

  @media screen and (min-width: 768px) {
    h1 {
      font-size: 3rem;
    }

    h2 {
      font-size: 2.5rem;
    }

    h3 {
      font-size: 2rem;
    }

    h4 {
      font-size: 1.5rem;
    }

    h5 {
      font-size: 1.25rem;
    }

    h6 {
      font-size: 1rem;
    }
  }

  /* Set default paragraph styles */
  p {
    margin: 0;
    font-family: 'Oswald', sans-serif;
    font-size: 1rem;
    line-height: 1.5;
  }

  /* Set default list styles */
  ul,
  ol {
    margin: 0;
    padding: 0;
    list-style: none;
  }

  /* Set default input styles */
  input {
    border: none;
    outline: none;
    background-color: #f5f5f5;
    padding: 0.5rem 1rem;
    font-family: 'Roboto', sans-serif;
    font-size: 1rem;
  }

  /* Set default form styles */
  form {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }

  /* Set default section styles */
  section {
    margin: 0;
  }
`;
