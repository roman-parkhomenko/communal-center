import { Circle, LayerGroup, Popup, TileLayer } from "react-leaflet";
import { LatLngExpression } from "leaflet";

import "leaflet/dist/leaflet.css";
import { HighMapContainer } from "./map-card.styles";

const MapCard = () => {
  const position: LatLngExpression = [51.669014648593354, 33.91461843882907];
  return (
    <HighMapContainer center={position} zoom={15} scrollWheelZoom={false}>
      <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
      <LayerGroup>
        <Circle
          center={position}
          pathOptions={{ color: "#0d6a61" }}
          radius={30}
        >
          <Popup>Ми тут!</Popup>
        </Circle>
      </LayerGroup>
    </HighMapContainer>
  );
};
export default MapCard;
