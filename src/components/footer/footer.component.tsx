// import { ReactComponent as YtLogo } from '../../assets/youtube.svg';
// import { ReactComponent as InstagramLogo } from '../../assets/instagram.svg';
// import { ReactComponent as FbLogo } from '../../assets/facebook.svg';
import gnrLogo from "../../assets/logo-gmr.webp";

import { FooterContainer } from "./footer.styles";

const Footer = () => {
  return (
    <FooterContainer>
      <div>
        <a href="https://hlukhiv-rada.gov.ua/" target="_blank" rel="noreferrer">
          <img
            src={gnrLogo}
            alt="gmr logo"
            title="Перейти до офіційного сайту Глухівської міської ради"
          />
        </a>

        {/*<a*/}
        {/*  href="https://www.facebook.com/po.gnpu.edu.ua"*/}
        {/*  target="_blank"*/}
        {/*  rel="noreferrer"*/}
        {/*  title="Перейти до нашої сторінки у Facebook"*/}
        {/*>*/}
        {/*  /!*<FbLogo />*!/*/}
        {/*</a>*/}
        {/*<a*/}
        {/*  href="https://instagram.com/po_gnpu_1874"*/}
        {/*  target="_blank"*/}
        {/*  rel="noreferrer"*/}
        {/*  title="Перейти до нашої сторінки в Instagram"*/}
        {/*>*/}
        {/*  <InstagramLogo />*/}
        {/*</a>*/}
        {/*<a*/}
        {/*  href="https://youtube.com/@user-je8lj9qp4s"*/}
        {/*  target="_blank"*/}
        {/*  rel="noreferrer"*/}
        {/*  title="Перейти до сторінки у Youtube"*/}
        {/*>*/}
        {/*  <YtLogo />*/}
        {/*</a>*/}
      </div>
      <p>
        &copy; Copyright by Pavlik from Hlukhiv. 2023. &nbsp; Created by
        <a
          target="_blank"
          rel="noreferrer"
          href="https://roman-react-webstudio.netlify.app/"
          title="Go to webstudio website"
        >
          &nbsp;Roman Parkhomenko
        </a>
      </p>
    </FooterContainer>
  );
};
export default Footer;
