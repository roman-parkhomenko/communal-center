import styled from "styled-components";

export const FooterContainer = styled.footer`
  padding: 2rem 3rem;
  background: darkgray;
  box-shadow: 0px -15px 15px -15px rgb(0 0 0 / 80%);

  & > div {
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    align-items: center;
    z-index: 2;
    a {
      margin: 0 auto;
      max-width: 15%;
      img,
      svg {
        max-height: 120px;
        width: 100%;
      }
      &:hover {
        animation: grow 2s linear;

        &:focus {
          outline: none;
        }
      }
    }
  }
  p {
    text-align: left;
    z-index: 2;
    font-family: "Playfair Display", serif;
  }
  @keyframes grow {
    0% {
      transform: scale(1);
    }
    90% {
      transform: scale(1.05);
    }
    100% {
      transform: scale(1);
    }
  }
`;
