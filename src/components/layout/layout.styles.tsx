import styled from "styled-components";

export const LayoutContainer = styled.div`
  width: 100%;
  min-height: 100vh;
  margin: 0 auto;
  position: relative;
  scrollbar-gutter: auto;
  background-color: white;
`;
export const Main = styled.main`
  position: relative;
  padding: 5rem 0 2rem;
  height: 100%;
  min-height: calc(100vh - 5rem);
  overflow-x: hidden;
  z-index: 2;
`;
