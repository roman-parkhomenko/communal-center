import { Outlet } from "react-router-dom";
import Navigation from "../navigation/navigation.component";
import ScrollToTopOnNavigate from "../../utils/scroll-to-top-on-navigate";
import { GlobalStyles } from "../../global.styles";
import { LayoutContainer, Main } from "./layout.styles";
import { Suspense } from "react";
import Footer from "../footer/footer.component";
import ScrollUpBtn from "../UI/scroll-up-btn/scroll-up-btn.component";

const Layout = () => {
  return (
    <LayoutContainer>
      <GlobalStyles />
      <Navigation />
      <Main>
        <Suspense>
          <Outlet />
        </Suspense>
      </Main>
      <Footer />
      <ScrollToTopOnNavigate />
      <ScrollUpBtn />
    </LayoutContainer>
  );
};

export default Layout;
