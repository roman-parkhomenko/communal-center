import styled from "styled-components";
import { NavLink } from "react-router-dom";

export const NavBody = styled.div`
  display: flex;
  //height: 100vh;
  box-sizing: border-box;
  flex-direction: column;
  justify-content: center;
  align-content: center;
  font-family: "Open Sans", sans-serif;

  @media screen and (max-width: 767px) {
    background: #89535399;
    inset: 0;
    padding: 0 5rem;
  }
  @media screen and (min-width: 768px) {
    top: 5.5rem;
    right: 1.5rem;
    left: 50%;
  }
  position: fixed;
  z-index: 4;
`;
export const NaviLink = styled(NavLink)`
  color: #fff;
  text-decoration: none;
  font-weight: 300;

  &:focus,
  &:focus-within {
    outline: none;
  }

  &.active {
    color: aqua;
  }

  @media (max-width: 767px) {
    font-size: 1.4rem;
    padding: 1rem 0.5rem;
  }
`;

export const Navbar = styled.nav`
  background-image: linear-gradient(to right, #ed6ea0 0%, #ec8c69 100%);
  border-radius: 0.5rem;
  text-transform: uppercase;
  box-shadow: inset 0.1rem 0.1rem 0.5rem rgba(0, 0, 0, 0.15);
  transform: skewX(-10deg);

  > ol {
    display: flex;

    > li {
      position: relative;
      display: flex;
      align-items: center;
      justify-content: center;
    }
  }

  @media (min-width: 768px) {
    > ol {
      justify-content: space-around;

      &:hover,
      &:focus-within {
        > li:first-child {
          > ${NaviLink} {
            opacity: 0.6;
          }
        }
      }

      > li {
        flex: 1 1 0;

        &:last-child:after {
          content: "";
          position: absolute;
          top: 0;
          width: 100%;
          bottom: 0;
          margin-right: -50%;
          background: rgba(0, 0, 0, 0.1);
          border-radius: 0.5rem;
          z-index: 0;
          transition: 350ms cubic-bezier(1, 0.49, 0.09, 1.29) all;
          pointer-events: none;
        }

        ${Array.from(
          { length: 12 },
          (_, i) => `
              &:first-child:nth-last-child(${i}) ~ li:last-child:after {
                right: ${50 + 100 * (i - 1)}%;
              }
          `
        ).join("")}
        ${Array.from(
          { length: 12 },
          (_, i) => `
          &:first-child:nth-last-child( ${i} ),
          &:first-child:nth-last-child( ${i} ) ~ li{
           ${Array.from(
             { length: i },
             (_, j) => `
            &:nth-child( ${j} ):hover,
            &:nth-child( ${j} ):focus,
            &:nth-child( ${j} ):focus-within{
              ~ li:last-child:after{
                right: ${50 + 100 * (i - j)}%;
              }
              > ol ${NaviLink}{
                box-shadow: inset 0 0 0 3rem rgba(0, 0, 0, 0), inset 0 0 0 3rem rgba(237, 110, 160, ${
                  1 - (1 / i) * j
                });
                &:hover,
                &:focus,
                &:focus-within{
                  box-shadow: inset 0 0 0 3rem rgba(0, 0, 0, 0.1), inset 0 0 0 3rem rgba(237, 110, 160, ${
                    1 - (1 / i) * j
                  });
                }
              }
            }
            &:last-child:hover:after,
            &:last-child:focus:after,
            &:last-child:focus-within:after{
              right: 50%;
            }

            `
           ).join("")}
          }
        `
        ).join("")}
        &:first-child {
          > ${NaviLink} {
            opacity: 1;
          }
        }

        > ${NaviLink} {
          //padding: 2rem 0;
          padding: 1rem 0;
          font-size: 1.6vw;
          position: relative;
          width: 100%;
          text-align: center;
          z-index: 1;
          opacity: 0.7;
          transition: 250ms ease all;
        }

        > ol {
          position: absolute;
          top: 100%;
          left: 0;
          width: 100%;
          visibility: hidden;

          > li {
            margin-top: 0.5rem;
            transition: 250ms cubic-bezier(0.42, 0.83, 0.49, 1.35) transform;
            transform: scale(0);

            > ${NaviLink} {
              display: block;
              text-align: center;
              border-radius: 0.2rem;
              padding: 1rem 0.5rem;
              font-size: 1.4vw;
              background: #ec8c69;
              color: rgba(255, 255, 255, 0.6);
              transition: 250ms ease all;

              &:hover,
              &:focus,
              &:focus-within {
                box-shadow: inset 0 0 0 3rem rgba(0, 0, 0, 0.1);
                color: rgba(255, 255, 255, 1);
              }
            }
          }
        }

        &:hover,
        &:focus,
        &:focus-within {
          > ${NaviLink} {
            opacity: 1 !important;
          }

          > ol {
            visibility: visible;

            > li {
              transform: scale(1);

              &:first-child {
                transition-delay: 150ms;
              }
            }

            ${Array.from(
              { length: 12 },
              (_, k) => `
            > li:nth-child( ${k} ) {
              transition-delay: ${150 * k}ms;
            } 
            `
            ).join("")}
          }
        }
      }
    }
  }

  @media (max-width: 767px) {
    background-image: linear-gradient(to bottom, #ed6ea0 0%, #ec8c69 100%);

    > ol {
      flex-direction: column;

      > li {
        flex-direction: column;
        align-items: stretch;
        text-align: center;

        > ${NaviLink}:nth-last-child(2) {
          position: relative;

          &:after {
            content: "";
            position: absolute;
            top: 50%;
            margin-top: -0.25rem;
            right: 1.5rem;
            border-top: 0.5rem solid #fff;
            border-left: 0.3rem solid transparent;
            border-right: 0.3rem solid transparent;
            transition: 250ms ease all;
          }
        }

        &:focus-within,
        &:focus,
        &:hover {
          & > ${NaviLink}:nth-last-child(2):after {
            transform: rotate(180deg);
          }

          > ol {
            max-height: 12rem;
          }
        }

        > ol {
          background: rgba(0, 0, 0, 0.1);
          box-shadow: inset 0.1rem 0.1rem 0.5rem rgba(0, 0, 0, 0.15);
          overflow: hidden;
          max-height: 0;
          transition: 250ms ease all;

          > li {
            ${NaviLink} {
              display: block;
              font-size: 1.2rem;
            }
          }
        }
      }
    }
  }
`;
