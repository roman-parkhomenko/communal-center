import { Navbar, NavBody, NaviLink } from "./menu.styles";

const Menu = () => {
  return (
    <NavBody>
      <Navbar>
        <ol>
          <li>
            <NaviLink to="/">Home</NaviLink>
          </li>
          <li>
            <NaviLink to="about">About</NaviLink>
          </li>
          <li>
            <NaviLink to="services">Services</NaviLink>
            <ol>
              <li>
                <NaviLink to="/">Big Project</NaviLink>
              </li>
              <li>
                <NaviLink to="/">Bigger Project</NaviLink>
              </li>
              <li>
                <NaviLink to="/">Huge Project</NaviLink>
              </li>
            </ol>
          </li>
          <li>
            <NaviLink to="/">Portfolio</NaviLink>
            <ol>
              <li>
                <NaviLink to="/">Last Project</NaviLink>
              </li>
              <li>
                <NaviLink to="/">Second Project</NaviLink>
              </li>
              <li>
                <NaviLink to="/">First Project</NaviLink>
              </li>
            </ol>
          </li>
          <li>
            <NaviLink to="contact">Contact</NaviLink>
          </li>
        </ol>
      </Navbar>
    </NavBody>
  );
};
export default Menu;
