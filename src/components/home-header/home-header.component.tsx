import { Link } from "react-router-dom";
import { HomeCarouselWrapper, Section } from "./home-header.styles";
import { useAppSelector } from "../../store/hooks";
import { selectHomeImages } from "../../store/home-images/home-images.slice";
import { useEffect, useState } from "react";

const MovingSectionsContent = [
  {
    heading: "КЕРУЮЧА КОМПАНІЯ",
    description: "№ 1 у м. Глухові - це КП ЖКЦ",
    motivation: [
      "Довірте управління вашим будинком нам!",
      "Прозора система управління, унікальні професійні послуги з обслуговування спільної власності, організація роботи підрядників, контроль якості...",
    ],
    image: "photo_2023-04-19_21-01-49.jpg",
    imagePosition: false,
    link: { name: "НАШЕ ПІДПРИЄМСТВО", to: "about" },
  },
  {
    heading: "ПОТРІБНА ДОПОМОГА?",
    description: "МИ ГОТОВІ ПІДТРИМАТИ ВАС!",
    motivation: [
      "Фахівці КП ЖКЦ добре знають та розуміють ваші очікування, спроможні організувати і налагодити управління вашим будинком відповідно до норм, стандартів і правил.",
      "Ми орієнтовані на результат!",
    ],
    image: "home-1.webp",
    imagePosition: false,
    link: { name: "ЗВ'ЯЖІТЬСЯ З НАМИ", to: "contact" },
  },
  {
    heading: "НЕМА СВІТЛА У ПІД'ЇЗДІ? ",
    description: "МИ ДОПОМОЖЕМО!",
    motivation: [
      "Наші електрики справжні професіонали. Ми усунемо будь-які неполадки з електрикою у вашому будинку.",
      "Телефони аварійно-диспетчерської служби: 000-000-0000 або 066-433-5912",
    ],
    image: "photo_2023-04-30_10-26-34.jpg",
    imagePosition: false,
    link: { name: "НАШІ ПОСЛУГИ", to: "services" },
  },
  {
    heading: "ПОТЕКЛА ВОДА ДО ПІДВАЛУ?",
    description: "МИ ЗАВЖДИ ПОРУЧ!",
    motivation: [
      "Наші фахівці усунуть поломку або виконають ремонт будь-якої частини вашого дому швидко і якісно.",
      "Телефони аварійно-диспетчерської служби: 000-000-0000 або 066-433-5912",
    ],
    image: "photo_2023-04-30_10-26-42.jpg",
    imagePosition: false,
    link: { name: "НАШІ ПОСЛУГИ", to: "services" },
  },
];

const HomeHeaderCarousel = () => {
  const homeImages = useAppSelector(selectHomeImages);
  const [activeIndex, setActiveIndex] = useState(0);

  // Move to the next section every 5 seconds
  useEffect(() => {
    const intervalId = setInterval(() => {
      setActiveIndex(
        (activeIndex) => (activeIndex + 1) % MovingSectionsContent.length
      );
    }, 6000);
    return () => clearInterval(intervalId);
  }, [activeIndex]);
  return (
    <HomeCarouselWrapper>
      {MovingSectionsContent.map(
        (
          { heading, description, motivation, link, image, imagePosition },
          index
        ) => (
          <Section
            key={index}
            imgUrl={homeImages[image]?.url}
            bgImgSizeContain={imagePosition}
            translateX={-activeIndex * 100}
            isActive={Boolean(index === activeIndex)}
          >
            <h1>{heading}</h1>
            <h3>{description}</h3>
            <p>{motivation[0]}</p>
            <p>{motivation[1]}</p>
            <div>
              <Link to={link.to}>
                <span>{link.name}</span>
                <i>&#8594;</i>
              </Link>
            </div>
          </Section>
        )
      )}
    </HomeCarouselWrapper>
  );
};
export default HomeHeaderCarousel;
