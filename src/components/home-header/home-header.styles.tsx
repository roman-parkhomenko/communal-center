import styled, { keyframes } from "styled-components";

const slideInFromLeft = keyframes`
  from {
    opacity: 0;
    transform: translateX(-40%);
  }
  to {
    opacity: 1;
    transform: translateX(0);
  }
`;
const slideIn = keyframes`
  0% {
    transform: translateX(-100%);
    opacity: 0;
  }
  50% {
    transform: translateX(0%);
    opacity: 1;
  }
  100%{
    transform: translateX(-100%);
    opacity: 0;
  }
`;

type SectionProps = {
  imgUrl: string;
  bgImgSizeContain: boolean;
  translateX: number;
  isActive: boolean;
};
export const Section = styled.section<SectionProps>`
  background: center no-repeat url(${({ imgUrl }) => imgUrl});
  background-size: ${({ bgImgSizeContain }) =>
    bgImgSizeContain ? "contain" : "cover"};
  min-height: calc(50vh - 2.5rem);
  box-shadow: inset 50px 0px 62.5em 12.5em #00000099;
  text-shadow: 0 0 20px black;
  color: #eee;
  // display: ${({ isActive }) => (isActive ? "hidden" : "none")};
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: start;
  position: relative;
  overflow: hidden;
  width: 100%;
  flex-shrink: 0;
  transition: all 0.5s ease-in-out;
  transform: translateX(${({ translateX }) => translateX}%);
  padding: 0 5%;

  @media screen and (min-width: 768px) {
    min-height: calc(100vh - 5rem);
    padding-right: 15%;
  }
  @media screen and (min-width: 992px) {
    padding-right: 35%;
  }

  i {
    font-size: 2em;
  }

  h1 {
    font-size: 4em;
    animation-delay: 0.3s;
  }

  h3 {
    font-size: 2.5em;
    margin-bottom: 1rem;
    animation-delay: 0.4s;
  }

  p {
    font-size: 2em;
    animation-delay: 0.5s;
  }

  div {
    animation-delay: 0.6s;

    &:hover span {
      animation: ${slideIn} 1.5s infinite;
    }

    a {
      font-size: 2em;
      margin-top: 1rem;

      i {
        font-size: 1.5em;
      }
    }
  }

  h1,
  h3,
  p,
  div {
    opacity: 0;
    animation-duration: 1s;
    animation-fill-mode: forwards;
    animation-name: ${({ isActive }) => (isActive ? slideInFromLeft : "none")};
  }
`;
export const HomeCarouselWrapper = styled.div`
  display: flex;
  overflow: hidden;
  font-size: 8px;
  @media screen and (min-width: 768px) {
    font-size: 15px;
  }
  @media screen and (min-width: 992px) {
    font-size: 16px;
  }
  @media screen and (min-width: 1232px) {
    font-size: 18px;
  }
  @media screen and (min-width: 1400px) {
    font-size: 20px;
  }
`;
