import { GlobalStyles } from "../../global.styles";
import { GoHomeBtn, Section } from "./404.styles";

function Page404() {
  return (
    <Section>
      <GlobalStyles />
      <div className="container">
        <div className="glitch" data-text="🐌">
          🐌
        </div>
        <div className="glow">🐌</div>
        <p className="subtitle">Ламати - не зводити</p>
      </div>
      <div className="scanlines"></div>
      <div>
        <GoHomeBtn to="/" title="Перейти на головну сторінку">
          КП ЖКЦ
        </GoHomeBtn>
      </div>
    </Section>
  );
}
export default Page404;
