import styled, { keyframes } from "styled-components";
import { Link } from "react-router-dom";

const glitch = keyframes`
  1% {
    transform: rotateX(10deg) skewX(90deg);
  }
  2% {
    transform: rotateX(0deg) skewX(0deg);
  }
`;
const createClipPathKeyframes = () => {
  let clipPathKeyframes = "";
  let prevTop = 0;
  for (let i = 1; i <= 30; i += 1) {
    const currTop = Math.floor((i / 100) * 100);
    const currBottom = 100 - currTop;
    prevTop = currTop;
    clipPathKeyframes += `
      ${i * (1 / prevTop) * 100}% {
        --top: ${prevTop}px;
        --bottom: ${currBottom}px;
        clip-path: inset(var(--top) 0 var(--bottom) 0);
      }
    `;
  }
  return clipPathKeyframes;
};

const noise1 = createClipPathKeyframes();
const noise2 = keyframes`
${Array.from({ length: 30 }, (_, i) => {
  const top = Math.floor(Math.random() * 100);
  const bottom = Math.floor(Math.random() * (101 - top));
  const percentage = i * (1 / 30) * 100;
  return `
      ${percentage}% {
        clip-path: inset(${top}px 0 ${bottom}px 0);
      }
    `;
}).join("")}
`;

const fudge = keyframes`
  from {
    transform: translate(0px, 0px);
  }
  to {
    transform: translate(0px, 2%);
  }
`;

const glitch2 = keyframes`
  1% {
    transform: rotateX(10deg) skewX(70deg);
  }
  2% {
    transform: rotateX(0deg) skewX(0deg);
  }
`;

export const Section = styled.section`
  background: black;
  font-family: "Oswald", sans-serif;
  font-style: italic;
  height: 100vh;
  margin: 0;
  overflow: hidden;

  .container {
    position: absolute;
    transform: translate(-50%, -50%);
    top: 40%;
    left: 50%;
  }

  .glitch {
    color: rgb(223, 191, 191);
    position: relative;
    font-size: 9vh;
    animation: ${glitch} 5s 5s infinite;
  }

  .glitch::before {
    content: attr(data-text);
    position: absolute;
    left: -2px;
    text-shadow: -5px 0 magenta;
    background: black;
    overflow: hidden;
    top: 0;

    animation: ${noise1} 3s linear infinite alternate-reverse,
      ${glitch} 5s 5.05s infinite;
  }

  .glitch::after {
    content: attr(data-text);
    position: absolute;
    left: 2px;
    text-shadow: -5px 0 lightgreen;
    background: black;
    overflow: hidden;
    top: 0;
    line-height: 1.5;

    animation: ${noise2} 3s linear infinite alternate-reverse,
      ${glitch} 5s 5s infinite;
  }

  .scanlines {
    overflow: hidden;
    mix-blend-mode: difference;
  }

  .scanlines::before {
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;

    background: repeating-linear-gradient(
      to bottom,
      transparent 0%,
      rgba(255, 255, 255, 0.05) 0.5%,
      transparent 1%
    );

    animation: ${fudge} 7s ease-in-out alternate infinite;
  }

  .glow {
    font-size: 9vh;
    animation: ${glitch} 5s 5s infinite;
    text-shadow: 0 0 1000px rgb(223, 191, 191);
    color: transparent;
    position: absolute;
    top: 0;
  }

  .subtitle {
    font-family: "Rubik Glitch", cursive;
    font-weight: 100;
    font-size: 1.8vh;
    color: rgba(162, 151, 151);
    text-transform: uppercase;
    letter-spacing: 1em;
    text-align: center;
    position: absolute;
    left: -35%;
    animation: ${glitch2} 5s 5.02s infinite;
  }
`;
export const GoHomeBtn = styled(Link)`
  font-family: Rubik Glitch, cursive;
  color: rgb(199, 251, 81);
  position: absolute;
  width: 100vw;
  top: 5vh;
  text-align: center;
  font-size: 3rem;
  text-shadow: rgb(255, 129, 236) 0px 0px 20px;
  animation: ${glitch2} 5s 5.02s infinite;
`;
