import { useState, MouseEvent, useEffect } from "react";
import {
  Header,
  Logo,
  LogoContainer,
  LogoName,
  MenuBtn,
  MenuIcon,
  Nav,
  NavigateLink,
  NavItem,
  NavMenu,
  Splash,
} from "./navigation.styles";
import logo from "../../assets/carpenter-at-work.webp";
import { sleep } from "../../App";
import Backdrop from "../UI/backdrop/backdrop.component";

const NavigationNew = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [mobileView, setMobileView] = useState(false);
  // const [offScreen, setOffScreen] = useState(false);
  // replace header company name on view width change
  useEffect(() => {
    function handleResize() {
      if (window.innerWidth < 992) {
        setMobileView(true);
      } else {
        setMobileView(false);
      }
    }
    // Set mobileView initially
    handleResize();

    // Add event listener for window resize
    window.addEventListener("resize", handleResize);

    // Cleanup function to remove event listener
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  //
  // useEffect(() => {
  //   window.addEventListener("scroll", () => {
  //     if (window.scrollY > window.innerHeight - 80) {
  //       setOffScreen(true);
  //     } else {
  //       setOffScreen(false);
  //     }
  //   });
  // }, []);
  const menuToggle = (e: MouseEvent<HTMLSpanElement>) => {
    e.preventDefault();
    setIsMenuOpen(!isMenuOpen);
  };
  const closeMenu = async () => {
    await sleep(300);
    setIsMenuOpen(false);
    return null;
  };
  return (
    <Header role="banner" data-aos="fade-down" data-aos-easing="linear">
      <LogoContainer>
        <Logo src={logo} alt="logo"></Logo>
        <LogoName to="/">
          {mobileView ? (
            <h1>КП ЖКЦ</h1>
          ) : (
            <>
              <h4>Комунальне підприємство</h4>
              <h2>Житловий комунальний центр</h2>
            </>
          )}
        </LogoName>
      </LogoContainer>
      <Nav isMenuOpen={isMenuOpen} role="navigation">
        <NavMenu
          aria-label="main navigation"
          isOpen={isMenuOpen}
          onClick={closeMenu}
        >
          <NavItem onClick={closeMenu}>
            <NavigateLink to="/">Головна</NavigateLink>
          </NavItem>
          <NavItem onClick={closeMenu}>
            <NavigateLink to="about">Про нас</NavigateLink>
          </NavItem>
          <NavItem onClick={closeMenu}>
            <NavigateLink to="services">Послуги</NavigateLink>
          </NavItem>
          <NavItem onClick={closeMenu}>
            <NavigateLink to="gallery">Галерея</NavigateLink>
          </NavItem>
          <NavItem onClick={closeMenu}>
            <NavigateLink to="contact">Контакти</NavigateLink>
          </NavItem>
        </NavMenu>

        <MenuBtn
          role="button"
          aria-expanded={isMenuOpen ? "true" : "false"}
          aria-controls="menu"
          onClick={menuToggle}
        >
          <MenuIcon
            xmlns="http://www.w3.org/2000/svg"
            width="50"
            height="50"
            viewBox="0 0 50 50"
          >
            <title>Toggle Menu</title>
            <g>
              <line x1="13" y1="16.5" x2="37" y2="16.5" />
              <line x1="13" y1="24.5" x2="37" y2="24.5" />
              <line x1="13" y1="24.5" x2="37" y2="24.5" />
              <line x1="13" y1="32.5" x2="37" y2="32.5" />
              <circle r="23" cx="25" cy="25" />
            </g>
          </MenuIcon>
        </MenuBtn>
        <Splash />
        {isMenuOpen && <Backdrop />}
      </Nav>
    </Header>
  );
};
export default NavigationNew;
