import styled, { css } from "styled-components";
import { Link, NavLink } from "react-router-dom";

export const MenuIcon = styled.svg`
  display: block;
  cursor: pointer;
  color: white;

  transform: rotate(0deg);
  transition: 0.3s cubic-bezier(0.165, 0.84, 0.44, 1);

  line,
  circle {
    fill: none;
    stroke: currentColor;
    stroke-width: 3;
    stroke-linecap: round;
  }
  line {
    transform: rotate(0deg);
    transform-origin: 50% 50%;
    transition: transform 0.25s ease-in-out;
  }
  circle {
    transition: stroke-dashoffset 0.3s linear 0.1s;
    stroke-dashoffset: circumference(23);
    stroke-dasharray: circumference(23);
  }
  @function circumference($r) {
    $pi: 3.141592653;
    @return 2 * $pi * $r;
  }
`;

export const Splash = styled.div`
  // ---------------------------
  // Circular Splash Background
  // ---------------------------
  position: absolute;
  top: 40px;
  right: 40px;
  width: 1px;
  height: 1px;

  &::after {
    content: "";
    display: block;
    position: absolute;
    border-radius: 50%;
    background-color: #0d6a61;
    // screen diameter can be 142vmax at most,
    // circle needs to be twice that size to cover it
    width: 284vmax;
    height: 284vmax;
    top: -142vmax;
    left: -142vmax;

    transform: scale(0);
    transform-origin: 50% 50%;
    transition: transform 0.5s cubic-bezier(0.755, 0.05, 0.855, 0.06);

    // will-change tells the browser we plan to
    // animate this property in the near future
    will-change: transform;

    @media screen and (min-width: 768px) {
      //changes menu appearance for nonMobile view
      width: 50vmax;
      height: 300vmax;
      top: -150vmax;
      left: -20vmax;
    }
  }
`;

export const MenuBtn = styled.span`
  display: inline-block;
  position: absolute;
  z-index: 10;
  padding: 0;
  border: 0;
  background: transparent;
  outline: 0;
  right: 15px;
  top: 15px;
  cursor: pointer;
  border-radius: 50%;
  transition: background-color 0.15s linear;

  &:hover,
  &:focus {
    background-color: rgba(0, 0, 0, 0.5);
  }
`;

export const NavigateLink = styled(NavLink)`
  color: white;
  display: block;
  text-align: center;
  text-transform: uppercase;
  letter-spacing: 5px;
  font-size: 1.25rem;
  text-decoration: none;
  padding: 1rem;
  line-height: 4vh;

  &:hover,
  &:focus {
    outline: 0;
    background-color: rgba(0, 0, 0, 0.2);
  }
`;

export const NavItem = styled.li`
  opacity: 0;
  transition: all 0.3s cubic-bezier(0, 0.995, 0.99, 1);

  &:nth-child(1) {
    transform: translateY(-40px);
    @media screen and (min-width: 768px) {
      display: none;
    }
  }

  &:nth-child(2) {
    transform: translateY(-80px);
    transition-delay: 0.25s;
  }

  &:nth-child(3) {
    transform: translateY(-120px);
    transition-delay: 0.3s;
  }

  &:nth-child(4) {
    transform: translateY(-160px);
    transition-delay: 0.35s;
  }

  &:nth-child(5) {
    transform: translateY(-200px);
    transition-delay: 0.4s;
  }
`;

export const NavMenu = styled.ul<{ isOpen: boolean }>`
  margin-top: -5rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: ${({ isOpen }) => (isOpen ? "100vh" : "0")};
  position: relative;
  z-index: 5;
  transition: height 0.3s cubic-bezier(0, 0.995, 0.99, 1) 0.5s;

  @media screen and (min-width: 768px) {
    align-items: flex-end;
  }
`;

const openMenuCss = css`
  //scale the background circle to full size
  ${Splash}::after {
    transform: scale(1);
  }

  //animate the menu icon
  ${MenuIcon} {
    color: white;
    transform: rotate(180deg);

    circle {
      stroke-dashoffset: 0;
    }
    line:nth-child(1),
    line:nth-child(4) {
      opacity: 0;
    }
    line:nth-child(2) {
      transform: rotate(45deg);
    }
    line:nth-child(3) {
      transform: rotate(-45deg);
    }
  }

  ${NavItem} {
    opacity: 1;
    transform: translateY(0);
  }
`;
export const Nav = styled.nav<{ isMenuOpen: boolean }>`
  &:target {
    ${openMenuCss}
  }
  ${({ isMenuOpen }) => isMenuOpen && openMenuCss}
`;
export const Header = styled.header`
  height: 5rem;
  background-color: #063636;
  top: 0;
  left: 0;
  right: 0;
  z-index: 100;
  position: fixed;
`;
export const LogoName = styled(Link)`
  color: #eee;

  h1 {
    font-size: 3rem;
    font-family: "Rubik 80s Fade", cursive;

    font-weight: normal;
  }
  h2,
  h4 {
    font-family: "Rubik 80s Fade", cursive;
    font-weight: normal;
    text-transform: uppercase;
  }
`;
export const LogoContainer = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
`;
export const Logo = styled.img`
  height: 100%;
`;
