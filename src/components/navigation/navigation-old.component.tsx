import { useEffect, useState } from "react";
import { NavLink, Link, useLocation } from "react-router-dom";
import styled from "styled-components";

type NavProps = {
  offScreen: boolean;
};
const HeaderContainer = styled.header<NavProps>`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #fff;
  height: 80px;
  padding: 0 24px;

  top: 0;
  left: 0;
  right: 0;
  z-index: 100;
  position: ${({ offScreen }) => (offScreen ? `fixed` : `absolute`)};

  @media screen and (min-width: 768px) {
    height: 100px;
    padding: 0 64px;
  }
`;

const MainLogo = styled(Link)`
  font-size: 24px;
  font-weight: bold;
  color: #000;
  text-decoration: none;
`;

type NavMenuProps = {
  active: boolean;
};
const NavMenu = styled.nav<NavMenuProps>`
  display: ${({ active }) => (active ? "flex" : "none")};
  flex-direction: column;
  position: absolute;
  top: 80px;
  left: 0;
  width: 100%;
  background-color: #fff;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
  padding: 24px;
  box-sizing: border-box;

  @media screen and (min-width: 768px) {
    position: initial;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    width: auto;
    background-color: initial;
    box-shadow: none;
    padding: 0;
    display: flex !important;
  }
`;

const NaviLink = styled(NavLink)`
  padding: 10px;
  color: #333;
  text-decoration: none;
  font-weight: bold;
  transition: color 0.2s ease-in-out;

  &:hover {
    background-color: #f0f0f0;
    color: darkgreen;
  }

  &.active {
    font-weight: bold;
    color: darkgreen;
  }

  @media screen and (max-width: 768px) {
    &.active {
      font-weight: bold;
      color: #fff;
      background-color: darkgreen;
    }

    &:hover {
      background-color: darkgreen;
      color: #fff;
    }
  }
`;

const NavMenuButton = styled.button`
  background-color: darkgreen;
  color: #fff;
  border: none;
  padding: 12px;
  border-radius: 4px;
  font-size: 16px;
  font-weight: bold;
  cursor: pointer;

  @media screen and (min-width: 768px) {
    display: none;
  }
`;

const Navigation = () => {
  const [isActive, setIsActive] = useState(false);
  const [offScreen, setOffScreen] = useState(false);
  const { pathname } = useLocation();

  useEffect(() => {
    return () => {
      setIsActive(false);
    };
  }, [pathname]);

  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > window.innerHeight) {
        setOffScreen(true);
      } else {
        setOffScreen(false);
      }
    });
  }, []);

  const handleMenuToggle = () => {
    setIsActive(!isActive);
  };

  return (
    <HeaderContainer offScreen={offScreen}>
      <MainLogo to="/">КП ЖКЦ</MainLogo>
      <NavMenu active={isActive}>
        <NaviLink to="/about">Про нас</NaviLink>
        <NaviLink to="/services">Послуги</NaviLink>
        <NaviLink to="/gallery">Галерея</NaviLink>
        <NaviLink to="/contact">Зв'яжіться з нами</NaviLink>
      </NavMenu>
      <NavMenuButton onClick={handleMenuToggle}>
        {isActive ? "Закрити" : "М Е Н Ю"}
      </NavMenuButton>
    </HeaderContainer>
  );
};

export default Navigation;
