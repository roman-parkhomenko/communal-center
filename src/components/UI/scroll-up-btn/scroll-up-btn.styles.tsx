import styled, { keyframes } from "styled-components";

const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

const fadeOut = keyframes`
  from {
    opacity: 1;
  }
  to {
    opacity: 0;
  }
`;
export const BackToTop = styled.button<{ scrollTop: boolean }>`
  position: fixed;
  bottom: 2rem;
  right: 2rem;
  background-color: #0d6a6180;
  color: #fff;
  border: none;
  border-radius: 50%;
  padding: 1rem;
  display: flex;
  align-items: center;
  justify-content: center;
  opacity: 0;
  animation: ${({ scrollTop }) => (scrollTop ? fadeIn : fadeOut)} 0.3s
    ease-in-out forwards;
  transition: all 0.3s ease-in-out;
  z-index: 3;
  box-shadow: inset 0 0 20px -8px black;

  &:hover {
    cursor: pointer;
    background-color: #0d6a61d9;
  }

  svg {
    width: 2rem;
    height: 2rem;
    fill: #fff;
  }
`;
