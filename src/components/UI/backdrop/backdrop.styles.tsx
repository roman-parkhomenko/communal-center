import styled from "styled-components";
export const BackdropBtn = styled.div`
  z-index: -1;
  background: rgba(0, 0, 0, 0.5);
  position: fixed;
  height: 100vh;
  inset: 0;
`;
