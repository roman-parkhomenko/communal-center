import { BackdropBtn } from "./backdrop.styles";

const Backdrop = () => <BackdropBtn />;
export default Backdrop;
