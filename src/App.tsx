import React, { lazy, useEffect } from "react";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

import "./global.styles";
import Layout from "./components/layout/layout.component";
import { homeLoader } from "./pages/home/home.component";
import Aos from "aos";
import "aos/dist/aos.css";

const Home = lazy(() => import("./pages/home/home.component"));
const Page404 = lazy(() => import("./components/404/404.component"));
const About = lazy(() => import("./pages/about/about.component"));
const Contact = lazy(() => import("./pages/contact/contact.component"));
const Gallery = lazy(() => import("./pages/gallery/gallery.component"));
const Services = lazy(() => import("./pages/services/services.component"));
let router = createBrowserRouter([
  {
    path: "/",
    Component: Layout,
    ErrorBoundary: Page404,
    children: [
      {
        index: true,
        loader: homeLoader,
        Component: Home,
      },
      { path: "about", Component: About },
      { path: "services", Component: Services },
      { path: "gallery", Component: Gallery },
      { path: "contact", Component: Contact },
    ],
  },
]);

export default function App() {
  useEffect(() => {
    Aos.init({
      duration: 1200,
      easing: "ease-in-out-back",
      once: true,
    });
  }, []);
  return <RouterProvider router={router} fallbackElement={<Fallback />} />;
}

export function sleep(n: number = 5) {
  return new Promise((r) => setTimeout(r, n));
}

export function Fallback() {
  return <p>Performing initial data load</p>;
}
