import {
  createAsyncThunk,
  createSelector,
  createSlice,
  PayloadAction,
} from "@reduxjs/toolkit";
import { RootState } from "../store";
import {
  FolderContentsObject,
  getFolderContentsObject,
} from "../../utils/firebase";

type GalleryState = {
  value: FolderContentsObject;
  status: "idle" | "loading" | "failed";
};
const initialState: GalleryState = {
  value: {},
  status: "idle",
};
export const fetchGalleryObject = createAsyncThunk(
  "gallery",
  async () => await getFolderContentsObject("gallery/uncategorized")
);

export const gallerySlice = createSlice({
  name: "gallery",
  initialState,
  reducers: {},
  // The `extraReducers` field lets the slice handle actions defined elsewhere,
  extraReducers: (galleryBuilder) => {
    galleryBuilder
      .addCase(fetchGalleryObject.pending, (state) => {
        state.status = "loading";
      })
      .addCase(
        fetchGalleryObject.fulfilled,
        (state, action: PayloadAction<FolderContentsObject>) => {
          state.status = "idle";
          state.value = action.payload;
        }
      )
      .addCase(fetchGalleryObject.rejected, (state) => {
        state.status = "failed";
      });
  },
});

const selectGalleryReducer = (state: RootState): GalleryState => state.gallery;

export const selectGallery = createSelector(
  [selectGalleryReducer],
  (gallery) => gallery.value
);

export default gallerySlice.reducer;
