import {
  createAsyncThunk,
  createSelector,
  createSlice,
  PayloadAction,
} from "@reduxjs/toolkit";
import { RootState } from "../store";
import {
  FolderContentsObject,
  getFolderContentsObject,
} from "../../utils/firebase";

type HomeImagesState = {
  value: FolderContentsObject;
  status: "idle" | "loading" | "failed";
};
const initialState: HomeImagesState = {
  value: {},
  status: "idle",
};
export const fetchHomeImagesObject = createAsyncThunk(
  "homeImages",
  async () => await getFolderContentsObject("gallery/homepage")
);

export const homeImagesSlice = createSlice({
  name: "homeImages",
  initialState,
  reducers: {},
  // The `extraReducers` field lets the slice handle actions defined elsewhere,
  extraReducers: (homeImagesBuilder) => {
    homeImagesBuilder
      .addCase(fetchHomeImagesObject.pending, (state) => {
        state.status = "loading";
      })
      .addCase(
        fetchHomeImagesObject.fulfilled,
        (state, action: PayloadAction<FolderContentsObject>) => {
          state.status = "idle";
          state.value = action.payload;
        }
      )
      .addCase(fetchHomeImagesObject.rejected, (state) => {
        state.status = "failed";
      });
  },
});

const selectHomeImagesReducer = (state: RootState): HomeImagesState =>
  state.homeImages;

export const selectHomeImages = createSelector(
  [selectHomeImagesReducer],
  (homeImages) => homeImages.value
);

export default homeImagesSlice.reducer;
