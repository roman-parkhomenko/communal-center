import {
  createAsyncThunk,
  createSelector,
  createSlice,
  PayloadAction,
} from "@reduxjs/toolkit";
import { RootState } from "../store";
import {
  FolderContentsObject,
  getFolderContentsObject,
} from "../../utils/firebase";

type AboutImagesState = {
  value: FolderContentsObject;
  status: "idle" | "loading" | "failed";
};
const initialState: AboutImagesState = {
  value: {},
  status: "idle",
};
export const fetchAboutImagesObject = createAsyncThunk(
  "aboutImages",
  async () => await getFolderContentsObject("gallery/aboutPage")
);

export const aboutImagesSlice = createSlice({
  name: "aboutImages",
  initialState,
  reducers: {},
  // The `extraReducers` field lets the slice handle actions defined elsewhere,
  extraReducers: (aboutImagesBuilder) => {
    aboutImagesBuilder
      .addCase(fetchAboutImagesObject.pending, (state) => {
        state.status = "loading";
      })
      .addCase(
        fetchAboutImagesObject.fulfilled,
        (state, action: PayloadAction<FolderContentsObject>) => {
          state.status = "idle";
          state.value = action.payload;
        }
      )
      .addCase(fetchAboutImagesObject.rejected, (state) => {
        state.status = "failed";
      });
  },
});

const selectAboutImagesReducer = (state: RootState): AboutImagesState =>
  state.aboutImages;

export const selectAboutImages = createSelector(
  [selectAboutImagesReducer],
  (aboutImages) => aboutImages.value
);

export default aboutImagesSlice.reducer;
