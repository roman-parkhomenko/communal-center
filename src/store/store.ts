import { configureStore } from "@reduxjs/toolkit";
import galleryReducer from "./gallery/gallery-uncategirized.slice";
import homeImagesReducer from "./home-images/home-images.slice";
import aboutImagesReducer from "./about-images/about-images.slice";

export const store = configureStore({
  reducer: {
    gallery: galleryReducer,
    homeImages: homeImagesReducer,
    aboutImages: aboutImagesReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
  devTools: process.env.NODE_ENV === "development",
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
// export type AppThunk<ReturnType = void> = ThunkAction<
//   ReturnType,
//   RootState,
//   unknown,
//   Action<string>
// >;
